<?php

include_once "$racine/modele/Commentaires.inc.php";

// creation du menu burger
$menuBurger = array();
$menuBurger[] = Array("url"=>"./?action=detailResto","label"=>"detailResto");
$menuBurger[] = Array("url"=>"./?action=commentaire","label"=>"Comment");

$mettrecomment = false;
$msg="";
// recuperation des donnees GET, POST, et SESSION
if (isset($_POST["idR"])) {
    if ($_POST["idR"] != "" && $_POST["pseudoU"] != "" && $_POST["com"] != "") {
        $idR = $_POST["idR"];
        $pseudoU = $_POST["pseudoU"];
        $com = $_POST["com"];

        $ret = AddCommentByIdR($idR,$pseudoU,$com);
        if ($ret) {
            $mettrecomment = true;
        } else {
            $msg = "Veuillez mettre une critique d'abords. ";
        }
    }
 else {
    $msg="Veuillez donnez une critique d'abords.";
    }
}

if ($mettrecomment) {
    // appel du script de vue qui permet de gerer l'affichage des donnees
header('Location:./?action=detail&idR='.$_POST["idR"]);
} else {
    // appel du script de vue qui permet de gerer l'affichage des donnees
    $titre = "L'insertion de commentaire a eu un probléme";
    include "$racine/vue/entete.html.php";
    include "$racine/vue/vueDetailRestofail.php";
    include "$racine/vue/pied.html.php";
}

?>
