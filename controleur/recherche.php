<?php
include_once "$racine/modele/bd.resto.inc.php";
include_once "$racine/modele/bd.photo.inc.php";
include_once "$racine/modele/bd.recherche.php";
// creation du menu burger
$menuBurger = array();
if (isset($_POST['nomVille']))
{
    $nom = $_POST['nomVille'];
    $listeRestos = getRestoRecherche($nom);
}
else {
    $listeRestos = getRestos();
}



$titre = "recherche resto";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueRecherche.php";
include "$racine/vue/pied.html.php";
