<?php
include_once "$racine/controleur/recherche.php";
?>
<h1 id="rech">Recherche</h1>
<h2 id="rechV">Villes</h2>
<form action="./?action=recherche" method="POST">
             <input type="text" name="nomVille" placeholder="Dans quelle ville habitez-vous ?" />
</form>

<?php


for ($i = 0; $i < count($listeRestos); $i++) {

	$lesPhotos = getPhotosByIdR($listeRestos[$i]['idR']);
    ?>

    <div class="card">
		<div class="photoCard">
            <?php if (count($lesPhotos) > 0) { ?>
                <img src="photos/<?= $lesPhotos[0]["cheminP"] ?>" alt="photo du restaurant" />
            <?php } ?>
        </div>

        <div class="descrCard"><?php echo "<a href='./?action=detail&idR=" . $listeRestos[$i]['idR'] . "'>" . $listeRestos[$i]['nomR'] . "</a>"; ?>
            <br />
            <?= $listeRestos[$i]["numAdrR"] ?>
            <?= $listeRestos[$i]["voieAdrR"] ?>
            <br />
            <?= $listeRestos[$i]["cpR"] ?>
            <?= $listeRestos[$i]["villeR"] ?>
						<br />
						<?= $listeRestos[$i]["telR"] ?>
        </div>

		<!--
		<div class="tagCard">
			<ul id="tagFood">

				<li class="tag">
					<span class="tag">#</span>
				</li>

			</ul>
		</div>
		-->

    </div>

    <?php
  
}
?>
