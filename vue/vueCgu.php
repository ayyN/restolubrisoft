		<h1 id="CGU">Conditions générales d'utilisation du site r3st0</h1>
			<p>
			www.r3st0.fr est édité par la société <b>Crèsto</b>, propriété de Lionel Romain, Ewen Prigent, et Christophe Fontaine.
			</p>

			<p>
			Siège social : 173 Boulevard de Strasbourg, 94130 Nogent-sur-Marne<br>
			Directeur de la publication <strong>Lionel Romain</strong>
			</p>
			<p>
			Responsable technique application : <strong>Lionel Romain</strong><br> Responsable technique hebergement : <strong>Ewen Prigent</strong><br> Contact administratif du site : <strong>Christophe Fontaine</strong>
			</p>

		<h2 id="CGUaccept">Acceptation des conditions générales (<abbr title="conditions générales d'utilisation">CGU</abbr>)</h2>

			<p>L'utilisation du site <strong>www.r3st0.fr</strong> implique l'acceptation des conditions d'utilisations mentionnées ci-dessous.</p>

			<p>Une fois connecté et participant au site, l'utilisateur reconnait avoir lu l'intégralité des conditions générales d'utilisations présentes dans la section <abbr title="conditions générales d'utilisation">CGU</abbr> du site. Les conditions sont disponibles à l'adresse suivante : www.r3st0.fr/cgu</p>

			<p>La société Crèsto se réserve le droit de modifier à tout instant les présentes conditions générales. A chaque nouvelle version, les utilisateurs sont avertis par mail, et par un bandeau sur le site. Ainsi les utilisateurs peuvent et <i>doivent</i> se tenir à jour des modifications apportées.</p>

			<p>En cas de manquement aux <abbr title="conditions générales d'utilisation">CGU</abbr> la société Crèsto se réserve le droit de suspendre ou supprimer un compte utilisateur.</p>

		<h2> Description du service </h2>
			<p>www.r3st0.fr permet de consulter les fiches des restaurants contenus dans sa base de données. Les restaurants sont décrus à l'aide de caractéristiques précises. Celles-ci permettent d'effectuer des recherches précises. Les utilisateurs ont la possibilité d'émettre un avis personnel sur les repas qu'ils ont pu faire.</p>

			<p>Les restaurants ont la possibilité de <em>répondre</em> aux critiques reçues sur le site. De même les critiques sont relues par une équipe de modération interne à la société Crèsto.</p>
		<h3>Fonctionnalités utilisateurs</h3>
		<h4 id="CGUgene"> Conditions générales</h4>
			<p>L'inscription et la publication d'avis ou d'une note est réservée aux personnes <strong>majeures</strong>.</p>

			<p>Lors de la publication d'un avis l'utilisateur comprend qu'une modération sera effectuée <strong>avant</strong> que l'avis soit définitivement visible dur le site www.r3st0.fr.</p>

			<p> Sont <strong>exclus</strong> du droit de publication d'avis :
				</p><li>les restaurateurs</li>
				<li>les employés de la société Crèsto</li>
			<p></p>

			<p>Les utilisateurs doivent porter un avis objectif sur le restaurant.</p>

			<p>Dans les commentaires peuvent être évalués :
				</p><li>la qualité gustative</li>
				<li>la propreté</li>
				<li>la diversité des plats</li>
				<li>le cout</li>
				<li>la qualité du service</li>
			<p></p>

			<p>Les jugements personnels autres <strong>ne peuvent</strong> être acceptés. De même il est <strong>interdit</strong> de faire de la publicité par quelque moyen que ce soit pour un restaurant concurrent dans les commentaires.</p>

			<p>Toute remarque sur l'origine ethnique, culturelle, l'orientation sexuelle, sera modérée, et le compte utilisateur pourra faire l'objet d'une suspension ou d'une suppression.</p>
		<h4 id="CGUavis">Modération des avis</h4>
			<p>Lorsque les avis donnés par les utilisateurs ne respectent pas les règles énoncées dans les <abbr title="Conditions générales d'utilisations">CGU</abbr>, l'avis peut être <strong>supprimé, masqué, ou modifié.</strong> Une fois modéré, l'utilisateur est averti, et peut le cas échéant modifier ou supprimer son commentaire.</p>
		<h4>Sanctions prévues en cas de non respect des règles</h4>
			<p>
				</p><ol>
					<li>suppression du commentaire</li>
					<li>suspension du compte</li>
					<li>suppression de tous les commentaires déjà postés, et des évaluations.</li>
					<li>suppression du compte</li>
				</ol>
			<p></p>
		<h4>Liste non exhaustive des motifs de modération des commentaires</h4>
			<p>
				</p><ul>
					<li>non respect des règles énoncées dans les <abbr title="Conditions générales d'utilisations">CGU</abbr> du site</li>
					<li>mention spécifique d'une personne</li>
					<li>spam</li>
					<li>commentaire posté par un robot ou généré</li>
					<li>texte illisible</li>
					<li>coordonnées personnelles</li>
					<li>lien vers un site extérieur</li>
					<li>diffamation</li>
					<li>inclusion de code javascript/SQL/PHP ou autre</li>
					<li>informations <em>personnelles</em> (numéro de téléphone, adresse, mail, IP, numéro de carte bancaire...)</li>
				</ul>
			<p></p>
		<h3>Fonctionnalités restaurateurs</h3>
		<h4 id="CGUgeneral">Généralités</h4>
		<h4>Protection des données personnelles</h4>
			<p>Lors de l'inscription certaines données personnelles sont stockées dans le système d'information de Crèsto. Ces données sont enregistrées conformément au respect de la protection des données personnelles. Ces données sont utilisées pour permettre à l'utilisateur de profiter de toutes les fonctionnalités du sute www.r3st0.fr</p>
		<h4>Droit d'accès et de modification</h4>
			<p>Les utilisateur enrefistrés peuvent en accédant à la page "mon compte" modifier les informations personnelles à défaut de leur adresse mail.</p>
		<h4>Utilisation de données par des partenaires</h4>
			<p>Lors de l'inscriptiion sur le site, les utilisateurs peuvent accepter ou refuser l'utilisation de leur adresse e-mail à des fins d'information (<em>newsletter</em>) pour le site www.r3st0.fr et d'autres sites de la société Crèsto.
			<br>
			Le carnet d'adresse ainsi constitué par la société Crèsto peut éventuellement être vendu à des sociétés partenaires, puis utilisé à des fins commerciales pour du démarchage, et pour orienter l'utilisateur vers des produits répondant à ses préférences.</p>
