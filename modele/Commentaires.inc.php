<?php
include_once "bd.inc.php";

function getCommentairesByIdR($idR) {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from Commentaires where idR=:idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}
?>
<?php
function AddCommentByIdR($idR,$idCom,$pseudoU,$com) {
    try {
        $cnx = connexionPDO();

        $req = $cnx->prepare("insert into Commentaires (idR,pseudoU,com) values(:idR, :pseudoU,:com)");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->bindValue(':pseudoU', $pseudoU, PDO::PARAM_STR);
        $req->bindValue(':com', $com, PDO::PARAM_STR);

        $resultat = $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return $resultat;
}
?>
