<?php

// connexion à la BDD
include_once "bd.inc.php";

function getRestoRecherche($nomVille) {


//Récupération du nom de la ville entré dans la zone de recherche
//$nomVille = isset($_GET['nomVille']) && !empty($_GET['nomVille'])?$_GET['nomVille']:"";

// création du WHERE
$strWhere = $nomVille ? " WHERE villeR like '%$nomVille%' " : '';

// Requête :
$resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from resto" .$strWhere);
        $req->execute();

        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        while ($ligne) {
            $resultat[] = $ligne;
            $ligne = $req->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}
?>
